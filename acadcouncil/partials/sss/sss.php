



<!-- <div class="containerdata">
 -->
<!-- <div class="sidenavbar">
<nav>
<ul>
<a ui-sref="sss_tsc"><li>Tutorials Service Center</li></a>
<a href="http://gymkhana.iitb.ac.in/~ugacademics/ug_acads/bookbay/" target="_blank"><li>Book Bay</li></a>
<a href="http://gymkhana.iitb.ac.in/~ugacademics/ug_acads/courserank/" target="_blank"><li>Course Rank</li></a>
<a href="http://gymkhana.iitb.ac.in/~ugacademics/wiki/index.php/Main_Page" target="_blank"><li>UG Acads Wiki Page</li></a>
<a ui-sref="sss_qng"><li>Query and Grievance portal</li></a>
<a href="http://gymkhana.iitb.ac.in/~smp/index.php" target="_blank"><li>Institute Student Mentor Programme</li></a>
<a ui-sref="sss_team"><li>Team</li></a>
</ul>
</nav>
</div> -->

<div class="sidenavbar">
<nav class="{{active}}" >
<ul>
<a ui-sref="sss"><li class="navheader">SSS</li></a>

<a ui-sref="sss_tsc">
<li class="tsc" ng-click="active='tsc'">
<div class="nav_images">
<img src="img/search_dark.png" alt="About">
</div>
<div class="nav_fields">TSC</div>
</li>
</a>

<a href="http://gymkhana.iitb.ac.in/~smp/index.php" target="_blank">
<li >
<div class="nav_images">
<img src="img/search_dark.png" alt="About">
</div>
<div class="nav_fields">ISMP</div>
</li>
</a>

<a href="http://gymkhana.iitb.ac.in/~ugacademics/ug_acads/bookbay/" target="_blank">
<li >
<div class="nav_images">
<img src="img/search_dark.png" alt="About">
</div>
<div class="nav_fields">Book Bay</div>
</li>
</a>

<a href="http://gymkhana.iitb.ac.in/~ugacademics/wiki/index.php/Main_Page" target="_blank">
<li >
<div class="nav_images">
<img src="img/search_dark.png" alt="About">
</div>
<div class="nav_fields">Wiki Page</div>
</li>
</a>



<!-- <a ui-sref="sss_qng">
<li class="qng" ng-click="active='qng'">
<div class="nav_images">
<img src="img/search_dark.png" alt="About">
</div>
<div class="nav_fields">QnG</div>
</li>
</a> -->

<!-- <a ui-sref="sss_team">
<li class="team" ng-click="active='team'">
<div class="nav_images">
<img src="img/search_dark.png" alt="About">
</div>
<div class="nav_fields">Team</div>
</li>
</a> -->

<a href="http://gymkhana.iitb.ac.in/~ugacademics/ug_acads/courserank/" target="_blank">
<li >
<div class="nav_images">
<img src="img/search_dark.png" alt="About">
</div>
<div class="nav_fields">Course Rank</div>
</li>
</a>

</ul>
</nav>
</div>

<div ui-view>
<div class="datacomin">
<h1>Student Support Services</h1>
The council mainly takes care of the basic requirements of the students like tutorials, registration , counselling , personality development, Language and Communication skills centre, faculty- student relations, learning issues etc. The Student Support Services has many components entirely dedicated for each of the above mentioned purpose. Student Support Services division comprises of the Tutorial Services Centre, Online Query and Grievance portal, Counselling centre, GROW – Personality Development Classes, Language and Communication centre, Online portals : Book Bay and Course Rank.
</div>
</div>


<!-- </div>
 -->