



<div ui-view>




<h1>Institute Learning Programme</h1>


The Industrial Learning Program is the first of its kind and is being organised by Academic Council and Students’ Technical Bodies of IIT-Bombay. It is aimed at providing students with opportunities to apply their academic expertise in solving real time Industrial Problems.<br>
<h3> About the Event:</h3>
This is a podium where students of highest caliber will have to put on their thinking caps to solve problems which constantly bother today's industry and innovators. The problem statement will be floated according to the duration dictated by the firm depending on its complexity. All problem statements will have a common deadline leading to the first ever “Industry Day” at IIT Bombay. Industry Day will be a confluence of all the participating companies and the student teams will be evaluated by their respective company officials. It will also serve as a platform where the students and the companies get to interact and the students get an opportunity to prove their mettle.<br><br>

<h3>Updates</h3>
Important dates



Floating of problem statement:<br> <span style="font-weight:bold">10-12<sup>th</sup> February 2014</span><br>
Mid-term review:<br> <span style="font-weight:bold">Dependent on project span</span><br>
Final submission/ Industry Day: <br><span style="font-weight:bold">Last Week of March</span><br>


</div>






