myApp.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/ugacads");
  // $urlRouterProvider.when('/ugacads/career/about','/ugacads/career/about/vision');
    // $urlRouterProvider.when('/ugacads/career/genre','/ugacads/career/genre/choose');

  //
  // Now set up the states
  $stateProvider
      .state('home',{
        url : '/ugacads',
        templateUrl : 'partials/home.php',
        // controller : 'Home'
      })
      .state('career',{
        url : '/career',
        parent : 'home',
        templateUrl : 'partials/career/career.php',
        controller : 'career'
      })
      .state('career_about',{
        url : '/about',
        parent : 'career',
        templateUrl : 'partials/career/career_about.php',
        controller : 'career'
      })
      
      .state('career_about_vision',{
        url : '/vision',
        parent : 'career_about',
        templateUrl : 'partials/career/career_about_vision.php',
        // controller : 'Home'
      })
      .state('career_about_faqs',{
        url : '/faqs',
        parent : 'career_about',
        templateUrl : 'partials/career/career_about_faqs.php',
        controller : 'career_faqs',
      })



      .state('career_events',{
        url : '/events',
        parent : 'career',
        templateUrl : 'partials/career/career_events.php',
        controller : 'career'
      })
      .state('career_events_careertalks',{
        url : '/careertalks',
        parent : 'career_events',
        templateUrl : 'partials/career/career_events_careertalks.php',
        // controller : 'career_faqs',
      })
      .state('career_events_coreweekend',{
        url : '/coreweekend',
        parent : 'career_events',
        templateUrl : 'partials/career/career_events_coreweekend.php',
        // controller : 'career_faqs',
      })



      .state('career_resources',{
        url : '/resources',
        parent : 'career',
        templateUrl : 'partials/career/career_resources.php',
        controller : 'career'
      })

      .state('career_resources_core',{
        url : '/core',
        parent : 'career_resources',
        templateUrl : 'partials/career/career_resources_core.php',
        // controller : 'career_resources'
      })
      .state('career_resources_non-core',{
        url : '/non-core',
        parent : 'career_resources',
        templateUrl : 'partials/career/career_resources_noncore.php',
        // controller : 'career_resources'
      })
      .state('career_resources_entrepreneurship',{
        url : '/entrepreneurship',
        parent : 'career_resources',
        templateUrl : 'partials/career/career_resources_entre.php',
        // controller : 'career_resources'
      })
      .state('career_resources_apping',{
        url : '/apping',
        parent : 'career_resources',
        templateUrl : 'partials/career/career_resources_apping.php',
        controller : 'career_resources_apping'
      })
      .state('career_resources_finance',{
        url : '/finance',
        parent : 'career_resources',
        templateUrl : 'partials/career/career_resources_finance.php',
        // controller : 'career_resources'
      })
      .state('career_resources_scholarships',{
        url : '/scholarships',
        parent : 'career_resources',
        templateUrl : 'partials/career/career_resources_scholar.php',
        // controller : 'career_resources'
      })
      .state('career_resources_placements-internships',{
        url : '/placements-internships',
        parent : 'career_resources',
        templateUrl : 'partials/career/career_resources_pni.php',
        // controller : 'career_resources'
      })










      .state('career_genre',{
        url : '/genre',
        parent : 'career',
        templateUrl : 'partials/career/career_genre.php',
        controller : 'career'
      })
      .state('career_genre_choosing a career',{
        url : '/choosing a career',
        parent : 'career_genre',
        templateUrl : 'partials/career/career_genre_choose.php',
        // controller : 'Home'
      })
      .state('career_genre_jobs',{
        url : '/jobs',
        parent : 'career_genre',
        templateUrl : 'partials/career/career_genre_jobs.php',
        // controller : 'Home'
      })
      .state('career_genre_entrepreneurship',{
        url : '/entrepreneurship',
        parent : 'career_genre',
        templateUrl : 'partials/career/career_genre_entre.php',
        // controller : 'Home'
      })


      .state('sss',{
        url : '/sss',
        parent : 'home',  
        templateUrl : 'partials/sss/sss.php',
        // controller : 'Home'
      })
      .state('sss_tsc',{
        url : '/tsc',
        parent : 'sss',
        templateUrl : 'partials/sss/sss_tsc.php',
        // controller : 'Home'
      })
      .state('sss_qng',{
        url : '/qng',
        parent : 'sss',
        templateUrl : 'partials/sss/sss_qng.php',
        // controller : 'Home'
      })
      .state('sss_team',{
        url : '/team',
        parent : 'sss',
        templateUrl : 'partials/sss/sss_team.php',
        // controller : 'Home'
      })


      .state('enpower',{
        url : '/enpower',
        parent : 'home',
        templateUrl : 'partials/enpower/enpower.php',
        // controller : 'Home'
      })
      .state('enpower_team',{
        url : '/team',
        parent : 'enpower',
        templateUrl : 'partials/enpower/enpower_team.php',
        // controller : 'Home'
      })
      .state('enpower_idp',{
        url : '/idp',
        parent : 'enpower',
        templateUrl : 'partials/enpower/enpower_idp.php',
        // controller : 'Home'
      })
      .state('enpower_ispa',{
        url : '/ispa',
        parent : 'enpower',
        templateUrl : 'partials/enpower/enpower_ispa.php',
        // controller : 'Home'
      })
      .state('enpower_ilp',{
        url : '/ilp',
        parent : 'enpower',
        templateUrl : 'partials/enpower/enpower_ilp.php',
        // controller : 'Home'
      })
    // .state('state1.list', {
    //   url: "/list",
    //   templateUrl: "partials/state1.list.html",
    //   controller: function($scope) {
    //     $scope.items = ["A", "List", "Of", "Items"];
    //   }
    // })
    // .state('state2', {
    //   url: "/state2",
    //   templateUrl: "partials/state2.html"
    // })
    // .state('state2.list', {
    //   url: "/list",
    //     templateUrl: "partials/state2.list.html",
    //     controller: function($scope) {
    //       $scope.things = ["A", "Set", "Of", "Things"];
    //     }
    //   })
    });






// 'use strict';

// angular.module('leafApp', [
//   'ngCookies',
//   'ngResource',
//   'ngSanitize',
// //   'ui.router'
// // ])
//   .config(function ($stateProvider,$urlRouterProvider) {

//     //$urlRouterProvider.when('/home/:room','/home/:room/index');

//     $stateProvider
//       .state('home',{
//         url : '/ugacads',
//         templateUrl : 'partials/home.php',
//         controller : 'Home'
//       })
//       .state('settings',{
//         url : '/settings',
//         parent : 'home',
//         templateUrl : 'views/home/settings.html',
//         controller : 'Sidesettingsmenu'
//       })

//       .state('setting-type',{
//         url : '/:type',
//         parent : 'settings',
//         templateUrl : 'views/home/usersettings.html',
//         controller : 'Usersettings'
//       })
     

//       .state('room',{
//         url : '/:rooms',
//         parent : 'home',
//         templateUrl : 'views/home/room.html',
//         controller : 'RoomCtrl'
//       })
//       .state('room-index',{
//         url : '/index',
//         parent : 'room',
//         templateUrl : 'views/home/room-index.html',
//         controller : 'RoomCtrl'
//       })
//       .state('reminder',{
//         parent : 'room',
//         url : '/reminders',
//         templateUrl : 'views/home/reminder.html',
//         controller : 'ReminderCtrl'
//       })
//       .state('edit-reminder',{
//         parent : 'room',
//         url : '/reminders/:id',
//         templateUrl : 'views/home/edit-reminder.html',
//         controller : 'EditreminderCtrl'
//       })
//       .state('action',{
//         url : '/target',
//         parent : 'room',
//         templateUrl : 'views/home/target.html',
//         controller : 'TargetCtrl'
//       })
//       .state('ind',{
//         url : '/',
//         templateUrl: 'views/login/login.html',
//         controller: 'LoginCtrl'
//       })
//       .state('create',{
//         url : '/create',
//         templateUrl: 'views/login/create.html',
//         controller: 'CreateCtrl'
//       })
//       .state('/forgot',{
//         url : '/forgot',
//         templateUrl: 'views/login/forgot.html',
//         controller: 'ForgotCtrl'
//       });
//   });