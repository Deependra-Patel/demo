'use strict';

  // myApp.controller('career_faqs ', function ($scope) {

  //   // $scope.usermenu = 0;

  //   // $scope.ShowMenu = function(){
  //   //   if($scope.usermenu === 0){
  //   //     $scope.usermenu = 1;
  //   //     console.log($scope.usermenu); 
  //   //   }
  //   //   else{
  //   //     $scope.usermenu = 0;
  //   //   }
  //   // };
    
  // });
  myApp.controller('career', ['$scope', '$http',
    function ($scope, $http){
        $scope.career =
          [
            "about","events","resources","genre"
          ];
        $scope.about =
          [
            "vision","faqs"
          ]; 
        $scope.events =
          [
            "careertalks","coreweekend"
          ]; 
        $scope.resources =
          [
            "apping","entrepreneurship","scholarships","core","non-core","finance","placements-internships"
          ];
        $scope.genre =
          [
            "choosing a career","jobs","entrepreneurship"
          ];  

    }]);
  myApp.controller('career_about', ['$scope', '$http',
    function ($scope, $http){
      $scope.heading =
        [
          "vision","faqs"
        ];         
    }]);
  myApp.controller('career_about', ['$scope', '$http',
    function ($scope, $http){
      $scope.heading =
        [
          "vision","faqs"
        ];         
    }]);
  myApp.controller('career_about', ['$scope', '$http',
    function ($scope, $http){
      $scope.heading =
        [
          "vision","faqs"
        ];         
    }]);

  myApp.controller('career_faqs', ['$scope', '$http',
  function ($scope, $http){
    $scope.datacalling = function(vara) {
      $http.get('json/career_faqs/career_faqs_'+vara+'.json').success(function(data){
          $scope.story = data;
      });
    } 
  }]);


  myApp.controller('career_resources_apping', ['$scope', '$http',
  function ($scope, $http){
    // $scope.datacalling = function(vara) {
    //   $http.get('json/career_resources/career_resources+'.json').success(function(data){
    //       $scope.heading =
    //           [
    //               "Introduction",
    //               "Help",
    //               "Apping Experience",
    //               "Apping FAQS"
    //           ];    
    //   });
    // } 
    $scope.heading =
              [
                  "Introduction",
                  "Help",
                  "Apping Experience",
                  "Apping FAQS"
              ];   

    
    $scope.appingdata = [
      {'heading': 'Introduction',
       'snippet': 'Fast just got faster with Nexus S.'},
      {'heading': 'Help',
       'snippet': 'The Next, Next Generation tablet.'},
      {'heading': 'Apping Experience',
       'snippet': 'The Next, Next Generation tablet.'},

      {'heading': 'Apping FAQS',
       'snippet': "What are the minimum requirements for apping?\n\n\nAns:1)You require around 1-2 internships or 3 good recommendations.2)You are required to give GRE and or TOEFL3)Be focussed to a limited number of fields.4)Decent CPI is required. More than 8 is nice and above 9 makes sure that you get a great app.What should you do if you are first year?Ans: You should concentrate on your coursework and introspect about your career goals.What should be my strategy if i am a second year student?Ans: Do an internship or a research project under URA( undergraduate research award) Internship either in IIT or abroad is good to get recos.I am a third year student, what should i do to get good apps?Ans: You should be ready to take the d\necision of apping or not. It is high time you give GRE/TOEFL. Do a research internship at the end of third year.What is the most important thing to do in 4th year for apping?Ans: You should seroiusly work on BTP and the apply for it.Ideally how many fields of interest should i concentrate on for securing a good app?Ans: You need upto 2 fields of interest to app. More the better but too much is also not good.How important are recommendation letters?Ans: You need to have atleast 3 recommendation letters, and if you have more it pays. There are various sources to get them like second year intern, third year intern, URA guide, BTP guide, Winter Internships, Seminar guide(if it applies to your department).What are the various components contributing towards getting an app?Ans: 1)CPI/Department Rank matters appreciably but it can be compensated with some good research .2)Recommendation Letters are really important stuff and one from a well known professor almost always gets you a good app3)Papers/Publications also weigh heavily on your application4)Internships play considerable role in deciding which app you get. Professor with whom your work is more important than the place you work in.5)Statement of Purpose(SOP) is not too important but a bad SOP can rreally hamper your chances. Make sure you consult seniors and write good English.6)GRE/TOEFL are also not that important for good universities but still aim to do good in these.What exactly is the process of apping?Ans: Number of univs to app to varies with your profile.App to enough\n\n places, but not too many as it Costs money, and can cause disadvantage to others A person with similar profile and higher CPI would invariably be chosen over you, unless they are picking many people Good univs don’t take too many people Try to have a different profile from others apping to the same place App to a few places where you think you wont get, just for fun but also app to a few places where you are sure you will get 10 should be an upper bound for CPI> 8.5, and 8-9 should be an upper bound for CPI > 9. Some myths related to Apping?Ans: 1) Extra curriculars matter.2) You need a +9 cpi to get into a top-6 place3) I can loaf off in my fourth year and still expect to get a good app.4) One must aim to publish at any cost.5) If I choose a field now, I have to do a PhD in it.6) Undergraduate time is for exploring out as many fields as possible.How should i decide whether to go to for higher studies or not?Ans: If you enjoy research, have deep interest in a particular field and want a career with more autonomy, confidently go for an app. But if you aim to make more money or want the prestige of another degree or are under family/friends pressure to app,better reconsider your decision.What is the differeence between M.S and Ph.D?Ans: M.S. is primarily a professional degree Master’s degree is an asset and a requirement for many industry jobs Teaching positions at community college level may also require an M.S.Ph.D. is a research degree Research and teaching positions in academia, industry and national labs.What are the various funding options available for higher studies?Ans: Ph.D. students are generally supported financially.M.S. students may or may not be supported (varies by program)Types of Funding: 1)Teaching Assistantships2)Research Assistantships3)Fellowships (usually for Ph.D.)4)Employer sponsorship (usually for M.S.)General fundaes for apping...Ans: It is important to first decide whether you want to app or not. It is not necessary that a high CPI is both a necessary and sufficient condition to app. Use this app site to gain basic background knowledge;after that, decide the field in which you would like to app and speak to two or three seniors in that field who will guide you through the entire process. Best of luck!"}
    ];      
  }]);




  // }

  // myApp.controller('career_resources_apping', function ($scope) {
  //   // $scope.engineer = {
  //   //     name: "Dani",
  //   //     currentActivity: "Fixing bugs"
  //   // };
 
    

