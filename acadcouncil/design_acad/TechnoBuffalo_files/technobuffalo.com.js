twoOhSix.config(
  {
    gpt: {
      slots:[
        {id: '/134702932/206_TB_BTF_Inactive', sz: [1, 1], div: 'ad-inactive-a', pb: false},
        {id: '/134702932/206_TB_ATF_NS_1x1', sz: [1, 1], div: 'ad-1x1-a', pb: false},
        {id: '/134702932/206_TB_ATF_Skinny', sz: [1, 1], div: 'ad-1x1-b', pb: false},
        {id: '/134702932/206_TB_ATF_Sliver_960x66', sz: [960, 66], div: 'ad-960x66-a', pb: false},
        {id: '/134702932/206_TB_BTF_WideBox', sz: [600, 300], div: 'ad-600x300-a', pb: false},
        {id: '/134702932/206_TB_ATF_Takeover', sz: [1, 1], div: 'ad-takeover-a', pb: false},
        {id: '/134702932/206_TB_BTFWebStories_BoxVertical', sz: [300, 380], div: 'ad-300x380-a', pb: false},

        {id: '/134702932/206_TB_ATF_Leaderboard', sz: [[728, 90],[970, 250]], div: 'ad-728x90-a', pb: false},
        {id: '/134702932/206_TB_ATF_Leaderboard_PB_A', sz: [728, 90], div: 'pb-728x90-a', pb: true},
        {id: '/134702932/206_TB_ATF_Leaderboard_PB_B', sz: [728, 90], div: 'pb-728x90-b', pb: true},
        {id: '/134702932/206_TB_ATF_Leaderboard_PB_A', sz: [970, 250], div: 'pb-970x250-a', pb: true},
        {id: '/134702932/206_TB_ATF_Leaderboard_PB_B', sz: [970, 250], div: 'pb-970x250-b', pb: true},

        {id: '/134702932/206_TB_BTF_Leaderboard', sz: [[728, 90],[970, 250]], div: 'ad-728x90-b', pb: false},
        {id: '/134702932/206_TB_BTF_Leaderboard_PB_A', sz: [728, 90], div: 'pb-728x90-c', pb: true},
        {id: '/134702932/206_TB_BTF_Leaderboard_PB_B', sz: [728, 90], div: 'pb-728x90-d', pb: true},
        {id: '/134702932/206_TB_BTF_Leaderboard_PB_A', sz: [970, 250], div: 'pb-970x250-c', pb: true},
        {id: '/134702932/206_TB_BTF_Leaderboard_PB_B', sz: [970, 250], div: 'pb-970x250-d', pb: true},

        {id: '/134702932/206_TB_BTF_Skyscraper', sz: [[160, 600],[300, 600]], div: 'ad-160x600-a', pb: false},
        {id: '/134702932/206_TB_BTF_Skyscraper_PB_A', sz: [160, 600], div: 'pb-160x600-a', pb: true},
        {id: '/134702932/206_TB_BTF_Skyscraper_PB_B', sz: [160, 600], div: 'pb-160x600-b', pb: true},
        {id: '/134702932/206_TB_BTF_Skyscraper_PB_A', sz: [300, 600], div: 'pb-300x600-i', pb: true},
        {id: '/134702932/206_TB_BTF_Skyscraper_PB_B', sz: [300, 600], div: 'pb-300x600-j', pb: true},

        {id: '/134702932/206_TB_ATF_Primary_Box', sz: [[300, 250],[300, 600]], div: 'ad-300x250-a', pb: false},
        {id: '/134702932/206_TB_ATF_Primary_Box_PB_A', sz: [300, 250], div: 'pb-300x250-a', pb: true},
        {id: '/134702932/206_TB_ATF_Primary_Box_PB_B', sz: [300, 250], div: 'pb-300x250-b', pb: true},
        {id: '/134702932/206_TB_ATF_Primary_Box_PB_A', sz: [300, 600], div: 'pb-300x600-a', pb: true},
        {id: '/134702932/206_TB_ATF_Primary_Box_PB_B', sz: [300, 600], div: 'pb-300x600-b', pb: true},

        {id: '/134702932/206_TB_BTF_Second_Box', sz: [300, 250], div: 'ad-300x250-b', pb: false},
        {id: '/134702932/206_TB_BTF_Second_Box_PB_A', sz: [300, 250], div: 'pb-300x250-c', pb: true},
        {id: '/134702932/206_TB_BTF_Second_Box_PB_B', sz: [300, 250], div: 'pb-300x250-d', pb: true},
        {id: '/134702932/206_TB_BTF_Second_Box_PB_A', sz: [300, 250], div: 'pb-300x600-c', pb: true},
        {id: '/134702932/206_TB_BTF_Second_Box_PB_B', sz: [300, 250], div: 'pb-300x600-d', pb: true},

        {id: '/134702932/206_TB_BTF_Third_Box', sz: [300, 250], div: 'ad-300x250-c', pb: false},
        {id: '/134702932/206_TB_BTF_Third_Box_PB_A', sz: [300, 250], div: 'pb-300x250-e', pb: true},
        {id: '/134702932/206_TB_BTF_Third_Box_PB_B', sz: [300, 250], div: 'pb-300x250-f', pb: true},

        {id: '/134702932/206_TB_BTF_Fourth_Box', sz: [300, 250], div: 'ad-300x250-d', pb: false},
        {id: '/134702932/206_TB_BTF_Fourth_Box_PB_A', sz: [300, 250], div: 'pb-300x250-g', pb: true},
        {id: '/134702932/206_TB_BTF_Fourth_Box_PB_B', sz: [300, 250], div: 'pb-300x250-h', pb: true},

        {id: '/134702932/206_TB_Mobile_ATF_320x50', sz: [320, 50], div: 'ad-320x50-mob-a', pb: false},
        {id: '/134702932/206_TB_Mobile_ATF_320x50_PB_A', sz: [320, 50], div: 'pb-320x50-mob-a', pb: true},
        {id: '/134702932/206_TB_Mobile_ATF_320x50_PB_B', sz: [320, 50], div: 'pb-320x50-mob-b', pb: true},

        {id: '/134702932/206_TB_Mobile_BTF_320x50', sz: [320, 50], div: 'ad-320x50-mob-b', pb: false},
        {id: '/134702932/206_TB_Mobile_BTF_320x50_PB_A', sz: [320, 50], div: 'pb-320x50-mob-c', pb: true},
        {id: '/134702932/206_TB_Mobile_BTF_320x50_PB_B', sz: [320, 50], div: 'pb-320x50-mob-d', pb: true},

        {id: '/134702932/206_TB_Mobile_ATF_300x250', sz: [300, 250], div: 'ad-300x250-mob-a', pb: false},
        {id: '/134702932/206_TB_Mobile_ATF_300x250_PB_A', sz: [300, 250], div: 'pb-300x250-mob-a', pb: true},
        {id: '/134702932/206_TB_Mobile_ATF_300x250_PB_B', sz: [300, 250], div: 'pb-300x250-mob-b', pb: true},

        {id: '/134702932/206_TB_Mobile_BTF_300x250', sz: [300, 250], div: 'ad-300x250-mob-b', pb: false},
        {id: '/134702932/206_TB_Mobile_BTF_300x250_PB_A', sz: [300, 250], div: 'pb-300x250-mob-c', pb: true},
        {id: '/134702932/206_TB_Mobile_BTF_300x250_PB_B', sz: [300, 250], div: 'pb-300x250-mob-d', pb: true},
      ]
    },
    outbrain:{
      placements:[
        {
          divId: 'bottom_teasers',
          widgets:[
            {widgetId: 'AR_1', template: 'technobuffalo'},
            {widgetId: 'AR_2', template: 'technobuffalo'},
            {widgetId: 'AR_3', template: 'technobuffalo'},
          ]
        }
      ]
    }
  }
);
