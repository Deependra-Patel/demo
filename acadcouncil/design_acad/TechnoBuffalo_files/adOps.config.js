tbAdConfig = [
{
	"slot": "/134702932/206_TB_ATF_Leaderboard",
	"sz": [[728, 90], [970, 90], [970, 250]],
	"id": "atf-leaderboard",
	"n_id": "ad-728x90-a"
},
{
	"slot": "/134702932/206_TB_ATF_Primary_Box",
	"sz": [[300, 250], [300, 600]],
	"id": "atf-primary-box",
	"n_id": "ad-300x250-a"
},
{
	"slot": "/134702932/206_TB_BTF_Second_Box",
	"sz": [[300, 250]],
	"id": "btf-second-box",
	"n_id": "ad-300x250-b"
},
{
	"slot": "/134702932/206_TB_BTF_Third_Box",
	"sz": [[300, 250]],
	"id": "btf-third-box",
	"n_id": "ad-300x250-c"
},
{
	"slot": "/134702932/206_TB_BTF_Fourth_Box",
	"sz": [[300, 250]],
	"id": "btf-fourth-box",
	"n_id": "ad-300x250-d"
},
{
	"slot": "/134702932/206_TB_BTF_Skyscraper",
	"sz": [[160, 600], [300, 600]],
	"id": "btf-skyscraper",
	"n_id": "ad-160x600-a"
},
{
	"slot": "/134702932/206_TB_BTF_WideBox",
	"sz": [[600, 300]],
	"id": "btf-box-wide",
	"n_id": "ad-600x300-a"
},
{
	"slot": "/134702932/206_TB_BTF_Leaderboard",
	"sz": [[728, 90]],
	"id": "btf-leaderboard",
	"n_id": "ad-728x90-b"
},
{
	"slot": "/134702932/206_TB_BTFWebStories_BoxVertical",
	"sz": [[300, 380]],
	"id": "btf-web-stories-vertical-box",
	"n_id": "ad-300x380-a"
},
{
	"slot": "/134702932/206_TB_ATF_Sliver_960x66",
	"sz": [[960, 66]],
	"id": "atf-skinny",
	"n_id": "ad-960x66-a"
},
{
	"slot": "/134702932/206_TB_ATF_Takeover",
	"sz": [[1, 1]],
	"id": "atf-takeover",
	"n_id": "ad-takeover-a"
},
{
	"slot": "/134702932/206_TB_BTF_Inactive",
	"sz": [[1, 1]],
	"id": "btf-inactive",
	"n_id": "ad-inactive-a"
},
{
	"slot": "/134702932/206_TB_ATF_NS_1x1",
	"sz": [[1, 1]],
	"id": "inter-ns",
	"n_id": "ad-1x1-a"
}
]