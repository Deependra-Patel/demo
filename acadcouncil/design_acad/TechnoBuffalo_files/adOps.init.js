// var tbDfp = {};
// (function(){
// 	var ads = {
// 		def: {
// 			config: tbAdConfig,
// 			units: ''
// 		},
// 		verify: function(self, def){
// 			if(typeof(def.config) !== 'undefined') {
// 				self.setup.base(self, def);
// 			}
// 		},
// 		setup: {
// 			base: function(self, def) {
// 				var config = def.config;
// 				tbFnPub.q = {};
// 				tbFnPub.q.a = [];
// 				tbFnPub.q.t = config.length;
// 				for(var i = 0; i < config.length; i++) {
// 					var ad = config[i];
// 					var id = ad.id;
// 					var slot = ad.slot;
// 					var sizes = ad.sz;
// 					tbDfp[id] = googletag.defineSlot(slot, sizes, id).addService(googletag.pubads());
// 					tbFnPub.q.a.push({i:id, s: slot, z: sizes});
// 				}
// 				self.setup.dfp()
// 			},
// 			dfp: function(){
// 				googletag.pubads().enableAsyncRendering();
// 				googletag.pubads().enableSingleRequest();
// 				googletag.pubads().collapseEmptyDivs();
// 				googletag.enableServices();
// 			}
// 		},
// 		init: function() {
// 			var self = this;
// 			var def = self.def;
// 			self.verify(self, def);
// 		}
// 	};
// 	ads.init();
// })();